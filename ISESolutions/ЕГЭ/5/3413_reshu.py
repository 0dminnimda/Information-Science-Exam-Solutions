i = 0

# 2x + 12 = 50
# x + 6 = 25
# x = 25 - 6

for i in range(25 - 6):
    i += 3
for i in range(25 - 6 + 12):
    i -= 2

print(i)