#!/usr/bin/python3.9
# -*- coding: utf-8 -*-

__version__ = "0.0.1"
__name__ = "ISESolutions"

from . import ЕГЭ

__all__ = (
    "__version__",
    "__name__",
    "ЕГЭ",)
